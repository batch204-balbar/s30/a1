db.fruits.insertMany([
    {
        "name": "Banana",
        "supplier": "Farmer Fruits Inc.",
        "stocks": 30,
        "price": 20,
        "onSale": true
    },
    {
        "name": "Mango",
        "supplier": "Mango Magic Inc.",
        "stocks": 50,
        "price": 70,
        "onSale": true
    },
    {
        "name": "Dragon Fruit",
        "supplier": "Farmer Fruits Inc.",
        "stocks": 10,
        "price": 60,
        "onSale": true
    },
    {
        "name": "Grapes",
        "supplier": "Fruity Co.",
        "stocks": 30,
        "price": 100,
        "onSale": true
    },
    {
        "name": "Apple",
        "supplier": "Apple Valley",
        "stocks": 0,
        "price": 20,
        "onSale": false
    },
    {
        "name": "Papaya",
        "supplier": "Fruity Co.",
        "stocks": 15,
        "price": 60,
        "onSale": true
    }
]);


// #2
db.fruits.aggregate([
        {
            $match: {
                "onSale": true
            }
        },
        {
            $count: "totalNumberOfFruitsOnSale"
        }
    ]);

// #3
db.fruits.aggregate([
        {
            $match: {
                "stocks": {
                    $gte: 20
                }
            }
        },
        {
            $count: "totalNumberOfFruitsStock>20"
        }
    ]);

// #4
db.fruits.aggregate([
        {
            $match: {
                "onSale": true,
                "supplier": {
                $in: ["Farmer Fruits Inc.", "Mango Magic Inc.", "Fruity Co.","Apple Valley"]
            }
        }
        },
        {
        $group: {
            _id: "$supplier", maxAmount: {$avg: "$price" }
        }
    }
    ]);

// 5
db.fruits.aggregate([
    {
        $match: {
            "supplier": {
                $in: ["Farmer Fruits Inc.", "Mango Magic Inc.", "Fruity Co.","Apple Valley"]
            }
        }
    },
    {
        $group: {
            _id: "$supplier", maxAmount: {$max: "$price" }
        }
    },
]);

// 6
db.fruits.aggregate([
    {
        $match: {
            "supplier": {
                $in: ["Farmer Fruits Inc.", "Mango Magic Inc.", "Fruity Co.","Apple Valley"]
            }
        }
    },
    {
        $group: {
            _id: "$supplier", maxAmount: {$min: "$price" }
        }
    },
]);